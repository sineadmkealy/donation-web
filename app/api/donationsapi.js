'use strict';

const Donation = require('../models/donation');
const Boom = require('boom');
const utils = require('./utils.js');
//-------------------------------------------------------------------------------------
exports.findAllDonations = {

  auth: {
    strategy: 'jwt',
  },

  handler: function (request, reply) {
    Donation.find({}).populate('donor').populate('candidate').then(donations => {
      reply(donations);
    }).catch(err => {
      reply(Boom.badImplementation('error accessing db'));
    });
  },
};
//-------------------------------------------------------------------------------------
exports.findDonations = {

  auth: {
    strategy: 'jwt',
  },

  handler: function (request, reply) {
    Donation.find({ candidate: request.params.id }).then(donations => {
      reply(donations);
    }).catch(err => {
      reply(Boom.badImplementation('error accessing db'));
    });
  },

};
//-------------------------------------------------------------------------------------
/**
* When a donation is created, it populates the candidate field and
 *  initialises the donor field in a new donation.
 *  When creating a donation, it must return the new donation including populated donor and candidate fields:
*/
exports.makeDonation = {

  auth: {
    strategy: 'jwt',
  },

  handler: function (request, reply) {
    const donation = new Donation(request.payload);
    donation.candidate = request.params.id;
    donation.donor = utils.getUserIdFromRequest(request);
    donation.save().then(newDonation => {
      return Donation.findOne(newDonation).populate('candidate').populate('donor');
    }).then(newDonation => {
      reply(newDonation).code(201);
    }).catch(err => {
      reply(Boom.badImplementation('error making donation'));
    });
  },

};
//-------------------------------------------------------------------------------------
/**
 * delete donations for a single candidate
 */
exports.deleteAllDonations = {

  auth: {
    strategy: 'jwt',
  },
  handler: function (request, reply) {
    Donation.remove({}).then(result => {
      reply().code(204);
    }).catch(err => {
      reply(Boom.badImplementation('error removing Donations'));
    });
  },
};
//-------------------------------------------------------------------------------------
/**
 * Delete all candidate donations
 */
exports.deleteDonations = {

  auth: {
    strategy: 'jwt',
  },

  handler: function (request, reply) {
    Donation.remove({ candidate: request.params.id }).then(result => {
      reply().code(204);
    }).catch(err => {
      reply(Boom.badImplementation('error removing Donations'));
    });
  },
};
/*
 exports.deleteAllCandidateDonations = {

 auth: false,
 handler: function (request, reply) {
 console.log(request.params.id);

 Donation.remove({ candidate: request.params.id }).then(err => {
 reply().code(204);
 }).catch(err => {
 reply(Boom.badImplementation('error removing all candidates Donations'));
 });
 },
 };*/
