/**
 * protect key routes so that they are only accessible to authenticated users.
 * These users must bear a valid token, which serves to validate the user and enable their identity to be confirmed.
 *
 * utility functions to generate and manipulate the tokens
 */

const jwt = require('jsonwebtoken');
const User = require('../models/user');

/**
 * need a way to authenticate the users, using email+password which, if correct, will lead us to generate a token.
 * generates a token
 */
exports.createToken = function (user) {
  return jwt.sign({ id: user._id, email: user.email }, 'secretpasswordnotrevealedtoanyone', {
    algorithm: 'HS256',
    expiresIn: '1h',
  });
};

/**
 * makes decoding tokens more convenient
 */
exports.getUserIdFromRequest = function (request) {
  var userId = null;
  try {
    const authorization = request.headers.authorization;
    var token = authorization.split(' ')[1];
    var decodedToken = jwt.verify(token, 'secretpasswordnotrevealedtoanyone');
    userId = decodedToken.id;
  } catch (e) {
    userId = null;
  }

  return userId;
};

/**
 * decodes an existing token, recovering the encrypted fields
 */
exports.decodeToken = function (token) {
  var userInfo = {};
  try {
    var decoded = jwt.verify(token, 'secretpasswordnotrevealedtoanyone');
    userInfo.userId = decoded.id;
    userInfo.email = decoded.email;
  } catch (e) {
  }

  return userInfo;
};

/**
 *  this function is required by the jwt validation strategy.
 *  It will be passed the decoded token and will attempt to validate it.
 *  It is valid if it contains an ID for user in the database.
 */
exports.validate = function (decoded, request, callback) {
  User.findOne({ _id: decoded.id }).then(user => {
    if (user != null) {
      callback(null, true);
    } else {
      callback(null, false);
    }
  }).catch(err => {
    callback(null, false);
  });
};
