/**
 * create a new module to represent a Schema for a User model:
 */

'use strict';

// establishes a connection to the database
const mongoose = require('mongoose');

/**
 * Everything in Mongoose starts with a Schema
 * Each schema maps to a MongoDB collection and defines the shape of the documents within that collection
 */
const userSchema = mongoose.Schema({
  firstName: String,
  lastName: String,
  email: String,
  password: String,
});

//
/**
 * Models are constructors compiled from Schema definitions:
 * Instances of these models represent documents which can be saved and retrieved from the database
 * All document creation and retrieval from the database is handled by these models
 *
 * User object can be used in other modules to interact with the “User” collection
 */
const User = mongoose.model('User', userSchema);

module.exports = User;