'use strict';

// establishes a connection to the database
const mongoose = require('mongoose'); //import mongoose
mongoose.Promise = global.Promise; //turn on native ES6 promises library instead

// declare the connection string
let dbURI = 'mongodb://localhost/donation-web';
//let dbURI = 'mongodb://donationuser:donationuser@ds023500.mlab.com:23500/donation-web';
if (process.env.NODE_ENV === 'production') {
  dbURI = process.env.MONGOLAB_URI;
}

mongoose.connect(dbURI); // connect to the database

/**
 * Log success/fail/disconnect
 * Mongoose Seeder Utility - to pre-load the database on startup
 * for a sample user + a donation
 */
mongoose.connection.on('connected', function () {
  console.log('Mongoose connected to ' + dbURI);
  if (process.env.NODE_ENV != 'production') {

    // When we define a new model, we must load it before attempting to seed the database
    var seeder = require('mongoose-seeder');
    const data = require('./inidata.json');
    const Donation = require('./donation');
    const User = require('./user');
    const Candidate = require('./candidate.js');
    seeder.seed(data, { dropDatabase: false, dropCollections: true }).then(dbData => {
      console.log('preloading Test Data');
      console.log(dbData);
    }).catch(err => {
      console.log(error);
    });
  }
});

mongoose.connection.on('error', function (err) {
  console.log('Mongoose connection error: ' + err);
});

mongoose.connection.on('disconnected', function () {
  console.log('Mongoose disconnected');
});