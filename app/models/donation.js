/**
 * Created by Sinead on 05-Oct-16.
 */

const mongoose = require('mongoose');

// equivalent to foreign key
const donationSchema = mongoose.Schema({
  amount: Number,
  method: String,
  donor: { //use an object reference directly to the User object
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
  },
    candidate:  {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Candidate',
  },
});

const Donation = mongoose.model('Donation', donationSchema);
module.exports = Donation;