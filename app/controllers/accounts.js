/**
 * Created by Sinead on 27-Sep-16.
 */
'use strict';

const User = require('../models/user'); // import the Model
const Joi = require('joi');

exports.main = {
  auth: false,//To disable the strategy for this route
  handler: function (request, reply) {
    reply.view('main', { title: 'Welcome to Donations' });
  },

};

exports.signup = {
  auth: false, // means password NOT required, authorisation not required on general pages only, NOT protected routes
  handler: function (request, reply) {
    reply.view('signup', { title: 'Sign up for Donations' });
  },

};

exports.login = {
  auth: false,
  handler: function (request, reply) {
    reply.view('login', { title: 'Login to Donations' });
  },

};

/**
 * Register HAPI Event Handler
 * Signup page action
 */
exports.register = {
  auth: false,

  validate: {

    payload: {
      firstName: Joi.string().required(),
      lastName: Joi.string().required(),
      email: Joi.string().email().required(),
      password: Joi.string().required(),
    },

    options: {
      abortEarly: false,
    },

    failAction: function (request, reply, source, error) {
      reply.view('signup', {
        title: 'Sign up error',
        errors: error.data.details,
      }).code(400);
    },

  },

  handler: function (request, reply) {
    const user = new User(request.payload);

    user.save().then(newUser => { //Save the Document (Promises): newUser is the saved object
      reply.redirect('/login'); // success : user saved successfully
    }).catch(err => { //an Error has occurred
      reply.redirect('/');
    });
  },

};
/**
 * authenticate HAPI event handler
 * to consult the database when validating a user
 * Login page action
 */
exports.authenticate = {

  auth: false,

  validate: {

    payload: {
      email: Joi.string().email().required(),
      password: Joi.string().required(),
    },

    options: {
      abortEarly: false,
    },

    failAction: function (request, reply, source, error) {
      reply.view('login', {
        title: 'Sign in error',
        errors: error.data.details,
      }).code(400);
    },

  },

  handler: function (request, reply) {
    const user = request.payload;
    User.findOne({ email: user.email }).then(foundUser => {
      if (foundUser && foundUser.password === user.password) {
        // Set the cookie if correct user credentials presented:
        // If cookie set, it can be read back in any handler
        request.cookieAuth.set({
          loggedIn: true,
          loggedInUser: user.email,
        });
        reply.redirect('/home');
      } else {
        reply.redirect('/signup');
      }
    }).catch(err => {
      reply.redirect('/');
    });
  },

};

//  clear the session
exports.logout = {
  auth: false,//Any attempt to access protected routes rejected
  handler: function (request, reply) {
    request.cookieAuth.clear();//Cookie deleted
    reply.redirect('/');
  },

};

//we need to read from the database to get the user details, and then
// //render these to the view (sending the user to the start page if there is an error):
exports.viewSettings = {

  handler: function (request, reply) {
    var userEmail = request.auth.credentials.loggedInUser;
    User.findOne({ email: userEmail }).then(foundUser => {// One email attribute we as searching on
      //DB Query success, check foundUser to see if match
      reply.view('settings', { title: 'Edit Account Settings', user: foundUser });
    }).catch(err => {
      reply.redirect('/');
    });
  },

};

/**
 * updateSettings HAPI event handler
 * Settings page action
 */
exports.updateSettings = {

  validate: {

    payload: {
      firstName: Joi.string().required(),
      lastName: Joi.string().required(),
      email: Joi.string().email().required(),
      password: Joi.string().required(),
    },

    options: {
      abortEarly: false,
    },

    failAction: function (request, reply, source, error) {
      reply.view('signup', {
        title: 'Sign up error',
        errors: error.data.details,
      }).code(400);
    },

  },

  handler: function (request, reply) {
    const editedUser = request.payload;
    const loggedInUserEmail = request.auth.credentials.loggedInUser;

    // To read a users details from the database, and
    // then update with new values entered by the user
    User.findOne({ email: loggedInUserEmail }).then(user => {//DB Query
      //Query succeeded, replace the fields:
      user.firstName = editedUser.firstName;
      user.lastName = editedUser.lastName;
      user.email = editedUser.email;
      user.password = editedUser.password;
      //Save the new version:
      return user.save(); // return a promise from the save() function -
      // and then re-render the updated user details to the settings view:
    }).then(user => {//New version saved
      reply.view('settings', { title: 'Edit Account Settings', user: user });
    }).catch(err => {
      reply.redirect('/');
    });
  },

};
