/**
 * Created by Sinead on 27-Sep-16.
 */

'use strict';

//the donate and report routes use this model
const User = require('../models/user');// import the Model
const Donation = require('../models/donation');// import the User model
const Candidate = require('../models/candidate');
const Joi = require('joi');

// refactor the donations controller to support making and listing donations
// switch to anonymous function .. to allow us access global variables later
// Handlers:
exports.home = {

  // always 2 parameters .. Node request
  handler: function (request, reply) {
    // sending back the view:
    // .file a method on object reply
    // equivalent to play 'render'
    // reply could have different methods depending on what returning
    // pass the list of candidates to the view to enable the user to select which candidate to make a donation to:
    Candidate.find({}).then(candidates => {
      reply.view('home', {
        title: 'Make a Donation',
        candidates: candidates,
      });
    }).catch(err => {
      reply.redirect('/');
    });
  },

};

/**
 *  donation schema model stores information about the donor
 *  Record donation + donor when creating donation
 *  when we create a donation, we will need to gain access to the object reference of the donor and use this reference to initialise the donation object
 * Creating a donation:  locate the current user in the database, also locate the preferred candidate.
 * Only when these two queries are performed can we create and insert a new donation object
 */
//an alternative, less heavily nested version:
exports.donate = {

  validate: {

    payload: {
      amount: Joi.number().required(),
      method: Joi.string().required(),
      candidate: Joi.string().required(),
    },

    options: {
      abortEarly: false,
    },

    failAction: function (request, reply, source, error) {
      Candidate.find({}).then(candidates => {
        reply.view('home', {
          title: 'Invalid Donation',
          candidates: candidates,
          errors: error.data.details,
        }).code(400);
      }).catch(err => {
        reply.redirect('/');
      });
    },
  },

  handler: function (request, reply) {
    var userEmail = request.auth.credentials.loggedInUser;
    let userId = null;
    let donation = null;
    User.findOne({ email: userEmail }).then(user => {
      let data = request.payload;
      userId = user._id;
      donation = new Donation(data);
      const rawCandidate = request.payload.candidate.split(',');
      return Candidate.findOne({ lastName: rawCandidate[0], firstName: rawCandidate[1] });
    }).then(candidate => {
      donation.donor = userId;
      donation.candidate = candidate._id;
      return donation.save();
    }).then(newDonation => {
      reply.redirect('/report');
    }).catch(err => {
      reply.redirect('/');
    });
  },

};

/**
 * Send all donations to the view
 */
exports.report = {

  handler: function (request, reply) {
    Donation.find({}).populate('donor').populate('candidate').then(allDonations => {
      let total = 0;
      allDonations.forEach(donation => {
        total += donation.amount;
      });
      reply.view('report', {
        title: 'Donations to Date',
        donations: allDonations,
        total: total,
      });
    }).catch(err => {
      reply.redirect('/');
    });
  },

};
