'use strict';

/**
 * Session tracking previoulsy by HttpSession, Cookies, hidden field, URL-rewriting
 * However, browser cookie facility is not appropriate for an API (where there may be no browser)
 * Use  JSON Web Tokens instead to secure the API.
 *
 *  objective is to protect key routes so that they are only accessible to authenticated users.
 *  These users must bear a valid token, which serves to validate the user and enable their identity to be confirmed.
 */

const Hapi = require('hapi');

// enable Cross Origin Resource Sharing with Donation-Client: use hapi-cors to make app accessible from an SPA
const corsHeaders = require('hapi-cors-headers');

var server = new Hapi.Server();
server.connection({ port: process.env.PORT || 4000 });

const utils = require('./app/api/utils.js');
require('./app/models/db');  //import of the mongo db module

server.register([require('inert'), require('vision'), require('hapi-auth-cookie'), require('hapi-auth-jwt2')], err => {

  if (err) {
    throw err;
  }

  server.views({
    engines: {
      hbs: require('handlebars'),
    },
    relativeTo: __dirname,
    path: './app/views',
    layoutPath: './app/views/layout',
    partialsPath: './app/views/partials',
    layout: true,
    isCached: false,
  });

  server.auth.strategy('standard', 'cookie', {
    password: 'secretpasswordnotrevealedtoanyone',
    cookie: 'donation-cookie',
    isSecure: false,
    // If route protected, and cookie deleted/ timeout:
    ttl: 24 * 60 * 60 * 1000,
    redirectTo: '/login', // Redirect protected Routes to Login Page
  });

  server.auth.strategy('jwt', 'jwt', {
    key: 'secretpasswordnotrevealedtoanyone',
    validateFunc: utils.validate,
    verifyOptions: { algorithms: ['HS256'] },
  });

  server.auth.default({
    strategy: 'standard',
  });

  server.ext('onPreResponse', corsHeaders);
  server.route(require('./routes'));
  server.route(require('./routesapi'));

  server.start((err) => {
    if (err) {
      throw err;
    }

    console.log('Server listening at:', server.info.uri);
  });

});
