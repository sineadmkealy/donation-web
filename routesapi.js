/**
 * routes file specifically to service the API route
 */
const CandidatesApi = require('./app/api/candidatesapi');
const UsersApi = require('./app/api/usersapi');
const DonationsApi = require('./app/api/donationsapi');

module.exports = [
  // candidates collection:
  { method: 'GET', path: '/api/candidates', config: CandidatesApi.find },//retrieves all candidate
  { method: 'GET', path: '/api/candidates/{id}', config: CandidatesApi.findOne },//retrieve a single candidate

  { method: 'POST', path: '/api/candidates', config: CandidatesApi.create }, //to create a single candidate
  { method: 'DELETE', path: '/api/candidates/{id}', config: CandidatesApi.deleteOne }, //to  delete a single candidate (d must be provided)
  { method: 'DELETE', path: '/api/candidates', config: CandidatesApi.deleteAll }, //deletes all candidates

  // users collection:
  { method: 'GET', path: '/api/users', config: UsersApi.find },
  { method: 'GET', path: '/api/users/{id}', config: UsersApi.findOne },
  { method: 'POST', path: '/api/users', config: UsersApi.create },
  { method: 'DELETE', path: '/api/users/{id}', config: UsersApi.deleteOne },
  { method: 'DELETE', path: '/api/users', config: UsersApi.deleteAll },

  // donations collection:
  { method: 'GET', path: '/api/donations', config: DonationsApi.findAllDonations },
  { method: 'GET', path: '/api/candidates/{id}/donations', config: DonationsApi.findDonations },//Creating a donation, and associating it with a candidate
  { method: 'POST', path: '/api/candidates/{id}/donations', config: DonationsApi.makeDonation },
  { method: 'DELETE', path: '/api/candidates/{id}/donations', config: DonationsApi.deleteDonations },
  { method: 'DELETE', path: '/api/donations', config: DonationsApi.deleteAllDonations },

  { method: 'POST', path: '/api/users/authenticate', config: UsersApi.authenticate },
];
